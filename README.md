# Test Network Speed

Just a simple code to test your download speed.

## Usage

Please check main.py for usage!

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[No License](https://choosealicense.com/licenses/unlicense/)